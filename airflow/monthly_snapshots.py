"""
Fetches and processes a weekly snapshot of Automoderator's config

Output:
- each snapshot is appended to wmf_product.automoderator_config
- and a latest snapshot is published as TSV to https://analytics.wikimedia.org/published/datasets/

Variables for testing 

{
  "start_date": "2024-09-14T15:00:00",
  "sla": "P1D",
  "alerts_email": "kcvelaga@wikimedia.org",
  "conda_env": "hdfs://analytics-hadoop/user/kcvelaga/artifacts/automoderator-metrics-jobs-0.1.0-v0.1.0.conda.tgz",
  "destination_table": "kcvelaga.automoderator_config_test",
  "spark_driver_memory": "2G",
  "spark_driver_cores": "4",
  "base_output_path": "hdfs://analytics-hadoop/tmp/kcvelaga/automoderator/archive_config",
  "temp_directory": "hdfs://analytics-hadoop/tmp/kcvelaga/automoderator/config"
}
"""

from datetime import datetime, timedelta

from airflow.operators.dummy import DummyOperator

from analytics_product.config.dag_config import (
    create_easy_dag,
    product_analytics_alerts_email,
    artifact,
    hadoop_name_node,
    hdfs_temp_directory
)

from wmf_airflow_common import util
from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.operators.spark import SparkSubmitOperator
from wmf_airflow_common.operators.hdfs import HDFSArchiveOperator


props = DagProperties(

    # start date, sla, and alerts email
    start_date=datetime(2024, 9, 19, 15),
    sla=timedelta(days=1),
    alerts_email=product_analytics_alerts_email,

    # conda env artifcator
    conda_env=artifact("automoderator-metrics-jobs-0.1.0-v0.1.0.conda.tgz"),
    destination_table="wmf_product.automoderator_config",

    # basic spark configuration
    spark_driver_memory="2G",
    spark_driver_cores="4",
    
    # paths for HDFSArchiveOperator
    # note: the intermediate directories will be created by the operator as needed
    base_output_path=f"{hadoop_name_node}/wmf/data/published/datasets/automoderator",
    temp_directory=f"{hadoop_name_node}{hdfs_temp_directory}/automoderator/config",
)

with create_easy_dag(
    dag_id="automoderator_config_weekly",
    doc_md="Updates the configuration of Automoderator on Wikimedia projects, weekly.",
    start_date=props.start_date,
    schedule="@weekly",
    tags=["weekly", "to_iceberg", "automoderator"],
    sla=props.sla,
    email=props.alerts_email
) as dag:
    
    start = DummyOperator(task_id="start")
    end = DummyOperator(task_id="end")

    output_file_path = props.base_output_path + "/" + "automoderator_config.tsv"

    spark_conf = {
        "spark.driver.memory": props.spark_driver_memory,
        "spark.driver.cores": props.spark_driver_cores,
    }

    # arguments to be passed into the extraction script
    # script at https://w.wiki/BF8f
    application_args= [
        "--destination_table",
        props.destination_table,
        "--output_path",
        props.temp_directory
    ]

    # writes to the destination table
    # stores a TSV in the temporary directory
    update_automoderator_config = SparkSubmitOperator.for_virtualenv(
        task_id="update_automoderator_config",
        virtualenv_archive=props.conda_env,
        entry_point="bin/extract_automoderator_config.py",
        conf=spark_conf,
        launcher="skein",
        application_args=application_args,
    )

    # publishes to https://analytics.wikimedia.org/published/datasets
    # clears up the temporary directory
    publish_tsv = HDFSArchiveOperator(
        task_id="publish_automoderator_config_tsv",
        source_directory=props.temp_directory,
        archive_file=output_file_path,
        expected_filename_ending=".csv",
        check_done=True
    )

    start >> update_automoderator_config >> publish_tsv >> end
import pytest

@pytest.fixture(name="dag_path")
def fixture_dagpath():
    return ["analytics_product", "dags", "automoderator", "automoderator_activity_snapshot_monthly_dag.py"]

def test_automoderator_snapshot_monthly_loaded(dagbag):
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id="automoderator_activity_snapshot_monthly")
    assert dag is not None
    # mwh_sensor, generate, write tsv, publish tsv, purge, complete
    assert len(dag.tasks) == 6

    # Tests that the defaults from default_args are here.
    assert dag.default_args["do_xcom_push"] is False

    # Tests that the defaults from dag_config are here.
    assert dag.default_args["metastore_conn_id"] == "analytics-hive"
