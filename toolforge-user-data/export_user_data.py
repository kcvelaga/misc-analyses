# attribution to https://gitlab.wikimedia.org/bd808; T386120
# https://wikitech.wikimedia.org/wiki/User:BryanDavis/LDAP

import ldap3
import yaml
import json

cfg = yaml.safe_load(open("/etc/ldap.yaml"))
conn = ldap3.Connection(cfg["servers"], auto_bind=True, read_only=True)
base = "ou=people,{basedn}".format(basedn=cfg["basedn"])
selector = "(&{})".format(
    "".join(
        [
            "(objectClass=posixAccount)",
            "(memberOf=cn=project-tools,ou=groups,dc=wikimedia,dc=org)",
        ]
    )
)

r = conn.extend.standard.paged_search(
    base,
    selector,
    attributes=[
        "uid",
        "cn",
        "createTimestamp",
        "wikimediaGlobalAccountId",
        "wikimediaGlobalAccountName",
        # "mail",
    ],
    paged_size=256,
    time_limit=5,
    generator=True,
)

data = []
for user in r:
    data.append(user)

with open('toolforge_user_data.json', 'w', encoding='utf-8') as f:
    json.dump(data, f, indent=4, default=str)
